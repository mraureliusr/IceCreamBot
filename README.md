# IceCreamBot

IceCreamBot is a Discord bot written in Python, based on the discord.py library which
abstracts the Discord API. Originally written just for fun and to learn a bit about
making a Discord bot, it developed into a whole new project when I attempted to find
a bot that could run Lua scripts and return the output. There were a few around, 
which supported a whole bunch of languages, but they either relied on APIs that had
been deprecated, they had little to no documentation, or the maintainers had long
abandoned them making them essentially useless.

I originally wrote this using disco-py, the *other* Discord Python library. However,
it has a fatal bug which makes this application impossible: it modifies messages 
before passing them to my function. It was removing whitespace and tabs, which, for
a language like Python, completely destroys the script as you cannot unambiguously
and automatically re-indent the code without knowing what it is supposed to do.
Instead of trying to solve this problem or track down the bug in the disco-py source
(something I wasted like 2-3 hours of my life trying to do) I just dropped it. 
Discord.py is much more widely used and supported anyway, so it has better (and more)
documentation, plus a larger community of users; which in turn results in more bots
and source code that you can read and learn from.

This bot, instead of depending on cloud computation for compilation, runs inside a
Docker container and actually runs your code locally inside the container. Yes, there
are some risks to allowing users to do this -- I highly recommend using permissions to
control which users can interact with the bot. There are some very, very basic safeguards
in place, but the best safeguard is the containerization of the app. If someone mananges
(either accidentally or intentionally) to blow things up, you can simply delete that
container and instantiate a new one from the container image. Again, we are relying on
the separation between Docker and the host OS, which is not as secure as a VM, but is
certainly better than just running it on the host OS directly.

If you want to install this bot, you need to [follow the instructions for setting up
Discord.py][1]. Essentially, on Debian-based systems, install  
`libffi-dev` and `python3.6-dev` first using your package manager. 
Change python3.6 to whichever version of Python
you are using (must be at least 3.6.5). Then, install discord.py using pip3: 

`pip3 install -U discord.py`

To actually run the bot, please follow the instructions on the [wiki page][2]

### TODO:
* Make the code execution much more secure (being worked on)
* Add support for ~~gcc~~, ~~ruby~~, perl?
* ~~Use pastebin links to circumvent the 2000 character limit of Discord messages~~
* Possibly add support for attachments? ie, to download compiled gcc binaries?
* Run the code as non-privelged user? This would require setup in Dockerfile

[1]: https://github.com/Rapptz/discord.py
[2]: https://gitlab.com/mraureliusr/IceCreamBot/wikis/home