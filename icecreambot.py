#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import random, datetime
import subprocess, re, requests
import os, dotenv

description = '''IceCreamBot parses Lua5.3, Ruby, Python3, and compiles C.'''

bot = commands.Bot(command_prefix='&', description=description)
ownerID = 239517706766909440
# create the regex needed later
stripRe = re.compile(r"`{1,3}(py(3)*|c|rb|ruby|lua)*")
pastebinRe = re.compile(r"^[\s]*#pb")
websiteRe = re.compile(r"(http|https):\/\/([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?")
allowedSitesRe = re.compile(r"(pastebin.com\/raw|termbin.com|hastebin.com\/raw)") # make sure they are the raw versions
disAllowedCRe = re.compile(r"(system|fopen|freopen|remove|rename)")
disAllowedPythonRe = re.compile(r"os.system|subprocess.(run|call)")
disAllowedLuaRe = re.compile(r"(os.execute|io.popen)")
disAllowedRubyRe = re.compile(r"exec")

async def parse(ctx, content):
    codeString = re.sub(stripRe, '', content)
    if ctx.author.id == ownerID: # owner can do whatever they want
        return codeString
    if pastebinRe.match(content) != None: # we use match to ensure it is the very first thing in the content
        try:
            url = websiteRe.search(content).group(0)
        except AttributeError:
            await ctx.send("That's not a valid URL! You donkey!")
            return "error"
        if allowedSitesRe.search(url) == None:
            await ctx.send("You must supply the raw version of the paste, or use an accepted website!")
            return
        website = requests.get(url)
        content = website.text
    if disAllowedLuaRe.search(content) != None or disAllowedRubyRe.search(content) != None or disAllowedPythonRe.search(content) != None or disAllowedCRe.search(content) != None:
        await ctx.send("Naughty! Time for a spanking...")
        return "error"
    return codeString

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.group()
async def math(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send("Invalid math command! Learn how to number.")

@math.command()
async def add(ctx, left : int, right : int):
    await ctx.send(left + right)

@bot.group()
async def run(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send("You must specify a language! You can choose gcc, py3, lua, or ruby. BUT NOT PERL!")

@run.command()
async def lua(ctx, *, content : str):
    await ctx.send("Parsing Lua...")
    codeString = await parse(ctx, content)
    if(codeString == "error"):
        return
    with open("/tmp/main.lua", "w") as openFile:
        openFile.write(codeString)
    try:
        result = subprocess.run(["lua5.3", "/tmp/main.lua"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=30)
    except subprocess.TimeoutExpired:
        await ctx.send("Timeout reached! Your code took too long to execute, dummy. Infinite loops are not good!\n")
        return
    resultDecoded = result.stdout.decode("UTF-8")
    print(result)
    if result.stdout != b'':
        if len(result.stdout) <= 1985: #fits in a single message
            await ctx.send("Result: ```\n" + resultDecoded[:1985] + "```")
        else: #more than three messages is a bit spammy
            await ctx.send("Output too long for Discord message! Are you printing a bloody book here?! Length: " + str(len(result.stdout)))
            with open("/tmp/termbin.txt", "w") as termbin:
                termbin.write(resultDecoded)
                catResult = subprocess.Popen(["cat", "/tmp/termbin.txt"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                termbinResult = subprocess.run(["netcat", "termbin.com", "9999"], stdin=catResult.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                catResult.wait()
                if termbinResult.stdout != b'':
                    await ctx.send("Output redirected to " + termbinResult.stdout.decode("UTF-8").strip("\0"))
                elif termbinResult.stderr != b'':
                    await ctx.send("Error redirecting to termbin: " + termbinResult.stderr.decode("UTF-8"))
                else:
                    await ctx.send("Huh.. that's strange. No output from netcat? Meow!")
        if result.stderr != b'': # sometimes running programs also throw errors
            await ctx.send("Error messages: ```\n" + result.stderr.decode("UTF-8") + "```")
    elif result.stderr != b'':
        await ctx.send("Errors: ```\n" + result.stderr.decode("UTF-8") + "```")
    else:
        await ctx.send("Hmm, no output... well that was pointless, wasn't it?")

@run.command()
async def py3(ctx, *, content : str):
    await ctx.send("Parsing Python 3...")
    codeString = await parse(ctx, content)
    if codeString == "error":
        return
    with open("/tmp/main.py", "w") as openFile:
        openFile.write(codeString)
    subprocess.run(["autopep8", "-i", "/tmp/main.py"])
    try:
        result = subprocess.run(["python3", "/tmp/main.py"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=30)
    except subprocess.TimeoutExpired:
        await ctx.send("Timeout reached! Your code took too long to execute. Please change it and try again!\n")
        return
    resultDecoded = result.stdout.decode("UTF-8")
    if result.stdout != b'':
        if len(result.stdout) <= 1985: #fits in a single message
            await ctx.send("Result: ```\n" + resultDecoded[:1985] + "```")
        else: #more than three messages is a bit spammy
            await ctx.send("Output too long for Discord message! Length: " + str(len(result.stdout)))
            await ctx.send("Redirecting output to termbin.com... Please wait.")
            with open("/tmp/termbin.txt", "w") as termbin:
                termbin.write(resultDecoded)
            catResult = subprocess.Popen(["cat", "/tmp/termbin.txt"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            termbinResult = subprocess.run(["netcat", "termbin.com", "9999"], stdin=catResult.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            catResult.wait()
            print(termbinResult)
            if termbinResult.stdout != b'':
                await ctx.send("Output redirected to " + termbinResult.stdout.decode("UTF-8").strip("\0"))
            elif termbinResult.stderr != b'':
                await ctx.send("Error redirecting to termbin: " + termbinResult.stderr.decode("UTF-8"))
            else:
                await ctx.send("Huh.. that's strange. No output from netcat?")
    elif result.stderr != b'':
        await ctx.send("Errors: ```\n" + result.stderr.decode("UTF-8") + "```")
    else:
        await ctx.send("Hmm, no output...")

@run.command()
async def ruby(ctx, *, content : str):
    await ctx.send("Parsing Ruby...")
    codeString = await parse(ctx, content)
    if codeString == "error":
        return
    with open("/tmp/main.rb", "w") as openFile:
        openFile.write(codeString)
    try:
        result = subprocess.run(["ruby", "/tmp/main.rb"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=30)
    except subprocess.TimeoutExpired:
        await ctx.send("Timeout reached! Your code took too long to execute. Please change it and try again!\n")
        return
    resultDecoded = result.stdout.decode("UTF-8")
    if result.stdout != b'':
        if len(result.stdout) <= 1985: #fits in a single message
            await ctx.send("Result: ```\n" + resultDecoded[:1985] + "```")
        else: #more than three messages is a bit spammy
            await ctx.send("Output too long for Discord message! Length: " + str(len(result.stdout)))
            await ctx.send("Redirecting output to termbin.com... Please wait.")
            with open("/tmp/termbin.txt", "w") as termbin:
                termbin.write(resultDecoded)
            catResult = subprocess.Popen(["cat", "/tmp/termbin.txt"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            termbinResult = subprocess.run(["netcat", "termbin.com", "9999"], stdin=catResult.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            catResult.wait()
            print(termbinResult)
            if termbinResult.stdout != b'':
                await ctx.send("Output redirected to " + termbinResult.stdout.decode("UTF-8").strip("\0"))
            elif termbinResult.stderr != b'':
                await ctx.send("Error redirecting to termbin: " + termbinResult.stderr.decode("UTF-8"))
            else:
                await ctx.send("Huh.. that's strange. No output from netcat?")
    elif result.stderr != b'':
        await ctx.send("Errors: ```\n" + result.stderr.decode("UTF-8") + "```")
    else:
        await ctx.send("Hmm, no output...")

@run.command()
async def gcc(ctx, *, content : str):
    await ctx.send("Compiling C...")
    codeString = await parse(ctx, content)
    if codeString == "error":
        return
    with open("/tmp/main.c", "w") as openFile:
        openFile.write(codeString)
    try:
        result = subprocess.run(["gcc", "-Wall", "-std=gnu11", "-o", "/tmp/main", "/tmp/main.c", "-lm"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=60)
    except subprocess.TimeoutExpired:
        await ctx.send("Timeout reached! Your code took too long to execute. Please change it and try again!\n")
        return
    resultDecoded = result.stdout.decode("UTF-8")
    resultErrDecoded = result.stderr.decode("UTF-8")
    print(result)
    if (result.stdout == b'' and result.returncode == 0) or (result.stdout != b'' and result.returncode == 1):
        await ctx.send("Compile messages: ```\n" + resultErrDecoded + "```")
        result = subprocess.run(["/tmp/main"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        resultDecoded = result.stdout.decode("UTF-8")
        if result.stdout != b'':
            if len(result.stdout) <= 1985: #fits in a single message
                await ctx.send("Result: ```\n" + resultDecoded[:1985] + "```")
            else: #more than three messages is a bit spammy
                await ctx.send("Output too long for Discord message! Length: " + str(len(result.stdout)))
                with open("/tmp/termbin.txt", "w") as termbin:
                    termbin.write(resultDecoded)
                catResult = subprocess.Popen(["cat", "/tmp/termbin.txt"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                termbinResult = subprocess.run(["netcat", "termbin.com", "9999"], stdin=catResult.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                catResult.wait()
                if termbinResult.stdout != b'':
                    await ctx.send("Output redirected to " + termbinResult.stdout.decode("UTF-8").strip("\0"))
                elif termbinResult.stderr != b'':
                    await ctx.send("Error redirecting to termbin: " + termbinResult.stderr.decode("UTF-8"))
                else:
                    await ctx.send("Huh.. that's strange. No output from netcat?")
        elif result.stderr != b'':
            await ctx.send("Error at runtime: ```\n" + result.stderr.decode("UTF-8") + "```")
        else:
            await ctx.send("Umm... no output.")
    elif result.stderr != b'' or result.returncode == 1:
        await ctx.send("Error at compile time: ```\n" + result.stderr.decode("UTF-8") + "```")
    else:
        await ctx.send("No output from compile?")

@run.command()
async def help(ctx):
    helpString = '''IceCreamBot v0.2 by MrAureliusR

To run code in Lua5.3, Python3.6, gcc -std=gnu11, or Ruby2.5.1, use the +run 
command followed by the lua, py3, gcc, and ruby respectively. After this, 
put your code, surrounded with either triple backticks or single backticks 
either side:

+run gcc ```
#include <stdio.h>

int main(void) {
    printf("Hello world!\\n");
    return 0;
}```

The bot will then run the code, and pass any error messages or programmatic output.
If the output is longer than a single Discord message (2000 characters), it will
redirect the output to termbin.com, a command-line accessible PasteBin, and give
you the link.

You can also pass code to the bot using PasteBin, HasteBin, or TermBin. Make sure 
you get the "raw" link from any of these sources, and then add the #pb tag to your
call:

+run py3 #pb https://pastebin.com/raw/xxxxxxxx

If your code hits an infinite loop, or takes longer than 30 seconds to run (90
seconds for gcc), it will silently kill your process so it doesn't lock up
the bot.'''
    await ctx.send(helpString)


dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
dotenv.load_dotenv(dotenv_path)

bot.run(os.environ['BOT_TOKEN']) # make sure to create a .env file with BOT_TOKEN in it
