FROM python:3.9-slim

COPY . /app
WORKDIR /app

RUN apt update
RUN apt install -y build-essential netcat lua5.3 libffi-dev ruby python-autopep8 procps
RUN pip install -r requirements.txt

CMD ["python3", "icecreambot.py"]
